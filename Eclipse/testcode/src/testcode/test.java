package testcode;
import java.util.ArrayList;
import java.util.List;
import java.lang.Math;

import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.optimization.PointValuePair;
import org.apache.commons.math3.optimization.direct.NelderMeadSimplex;
import org.apache.commons.math3.optimization.direct.SimplexOptimizer;
import org.apache.commons.math3.optimization.*;

@SuppressWarnings("deprecation")
public class test {
	public static ArrayList<float[]> pos;
	public static ArrayList<Double> dist;
	public static ArrayList<Double> dist_error;
	
	public static void main(String[] args) {
		pos = new ArrayList<float[]>();
		
		pos.add(new float[]{2.f, 2.f, .0f});
		pos.add(new float[]{.0f, 2.f, 1.f});
		pos.add(new float[]{2.f, .0f, 1.f});
		pos.add(new float[]{0.f, 0.f, 0.f});

		
        dist = new ArrayList<Double>();
        dist.add(1.45);
		dist.add(1.51);
		dist.add(1.57);
		dist.add(1.49);

		
		dist_error = new ArrayList<Double>();
		dist_error.add(-0.06);
		dist_error.add(-0.07);
		dist_error.add(-0.03);
		dist_error.add(-0.04);

		
        SimplexOptimizer optimizer = new SimplexOptimizer(1e-10, 1e-30);
        optimizer.setSimplex(new NelderMeadSimplex(new double[] { 0.2, 0.2, 0.2}));
        PointValuePair optimum
                = optimizer.optimize(500, new myfunc(), GoalType.MINIMIZE, new double[] { 2., 3., 0.3});
        double x = optimum.getPoint()[0];
        double y = optimum.getPoint()[1];
        double z = optimum.getPoint()[2];
        System.out.println("nelder result: "+ Double.toString(x) +","+ Double.toString(y) +"," + Double.toString(z));
		
		//bla(new double[]{3. ,  3. ,  0.3});
	}
	
/*	public static double bla(double x[]) {
		ArrayList<float[]> advers = pos;
		ArrayList<Double> dist_er = dist_error;
		ArrayList<Double> scaling = new ArrayList<Double>();
		
		List<float[]> keys = new ArrayList<float[]>(advers);
		ArrayList<Float> tmp = new ArrayList<Float>();

		for (float[] element: keys) {
			// Python: (x-beacon_positions)**2
			double new_x = Math.pow(x[0] - element[0], 2);
			double new_y = Math.pow(x[1] - element[1], 2);
			double new_z = Math.pow(x[2] - element[2], 2);
			
			
			// Python: (((x-beacon_positions)**2).sum(axis=1))
			double sum = new_x + new_y + new_z;
			
			// Python: ((x-beacon_positions)**2).sum(axis=1))**0.5
			tmp.add( (float) Math.pow(sum, 0.5) );
		}
		//tmp is now r
		
		
		for(int i = 0; i < tmp.size(); i++) {
			// Python: (s-r)**2
			double er = Math.pow(  dist.get(i) - tmp.get(i), 2 );
			
			// Python:  np.fmin(errors, 0*r+1**2)
			er = Math.min(er, 1.);
			tmp.set(i, (float) er);
		}
		// tmp is now error
		
		
//        scaling = []
//        for bv in beacon_variance:
//            if bv in [float, int, long]: scaling.append(1/bv)
//            else:                        scaling.append(1e-9)
//        scaling = np.array(scaling)
			
		for (Double e: dist_er) {
			// 1/0.0 will produce "Infinity"
			double scaling_tmp = 1/e.doubleValue();
			if (scaling_tmp == Double.NEGATIVE_INFINITY || scaling_tmp == Double.POSITIVE_INFINITY) {
				scaling_tmp = -0.000000001;
			}
			scaling.add( scaling_tmp );
		}
		
		double ret = 0.;
		for(int i = 0; i < tmp.size(); i++) {
			//System.out.println("ret: "+ Double.toString(ret));
			//System.out.println(tmp.get(i).toString() + "*" + scaling.get(i).toString() + "=" + (tmp.get(i) * scaling.get(i)));
			ret = ret + (tmp.get(i) * scaling.get(i));
			//System.out.println("ret: "+ Double.toString(ret));
		}
		
		System.out.println("ret: "+ Double.toString(ret));
		return ret;
	}*/
	
	
    private static class myfunc implements MultivariateFunction {
        /**
         * This should be error-function as of python-code
         * @param x
         * @return
         */
        public double value(double[] x) {
    		ArrayList<float[]> advers = pos;
    		ArrayList<Double> dist_er = dist_error;
    		ArrayList<Double> scaling = new ArrayList<Double>();
    		
    		List<float[]> keys = new ArrayList<float[]>(advers);
    		ArrayList<Float> tmp = new ArrayList<Float>();
    		
    		String dbg = "";
    		// Print out given x,y,z
    		for (int i = 0; i < x.length; i++) {
    			dbg += "x["+Integer.toString(i)+"]:" + Double.toString(x[i]) +" ";
    		}
    		System.out.println(dbg);
    		dbg = null;
    		
    		for (float[] element: keys) {
    			// Python: (x-beacon_positions)**2
    			double new_x = Math.pow(x[0] - element[0], 2);
    			double new_y = Math.pow(x[1] - element[1], 2);
    			double new_z = Math.pow(x[2] - element[2], 2);
    			
    			
    			// Python: (((x-beacon_positions)**2).sum(axis=1))
    			double sum = new_x + new_y + new_z;
    			
    			// Python: ((x-beacon_positions)**2).sum(axis=1))**0.5
    			tmp.add( (float) Math.pow(sum, 0.5) );
    		}
    		//tmp is now r
    		
    		for (Float e: tmp) {
    			System.out.println("r: " + Float.toString(e));
    		}
    		
    		
    		for(int i = 0; i < tmp.size(); i++) {
    			// Python: (s-r)**2
    			double er = Math.pow(  dist.get(i) - tmp.get(i), 2 );
    			
    			// Python:  np.fmin(errors, 0*r+1**2)
    			er = Math.min(er, 1.);
    			tmp.set(i, (float) er);
    		}
    		// tmp is now error
    		
    		
//            scaling = []
//            for bv in beacon_variance:
//                if bv in [float, int, long]: scaling.append(1/bv)
//                else:                        scaling.append(1e-9)
//            scaling = np.array(scaling)
    			
    		for (Double e: dist_er) {
    			// 1/0.0 will produce "Infinity"
    			double scaling_tmp = 1/e.doubleValue();
    			if (scaling_tmp == Double.NEGATIVE_INFINITY || scaling_tmp == Double.POSITIVE_INFINITY) {
    				scaling_tmp = -0.000000001;
    			}
    			scaling.add( scaling_tmp );
    		}
    		
    		double ret = 0.;
    		for(int i = 0; i < tmp.size(); i++) {
    			//System.out.println("ret: "+ Double.toString(ret));
    			//System.out.println(tmp.get(i).toString() + "*" + scaling.get(i).toString() + "=" + (tmp.get(i) * scaling.get(i)));
    			ret = ret + (tmp.get(i) * scaling.get(i));
    			//System.out.println("ret: "+ Double.toString(ret));
    		}
    		
    		System.out.println("ret: "+ Double.toString(ret));
    		return ret;
        }
    }
}
