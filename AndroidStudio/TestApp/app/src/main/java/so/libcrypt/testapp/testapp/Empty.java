package so.libcrypt.testapp.testapp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.optimization.PointValuePair;
import org.apache.commons.math3.optimization.direct.NelderMeadSimplex;
import org.apache.commons.math3.optimization.direct.SimplexOptimizer;
import org.apache.commons.math3.optimization.*;

import java.text.Format;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.lang.Math;

public class Empty extends Activity {
    public static final String LOGTAG = "31c3";
    private final static int REQUEST_ENABLE_BT = 1;
    // Initializes Bluetooth adapter.
    public BluetoothAdapter mBluetoothAdapter;

    private Hashtable<String, Advertiser> ADVERTISERS = new Hashtable<String, Advertiser>();

    private ArrayList<Float> s = new ArrayList<Float>(); //Distance
    private ArrayList<Float> beacon_variance = new ArrayList<Float>(); //Distance-Error
    private ArrayList<float[]> beacon_positions = new ArrayList<float[]>(); //Position
    private float[] sx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE not supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        Button button = (Button) findViewById(R.id.calc);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                get_position();
            }
        });


        mBluetoothAdapter.startLeScan(new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        String record = "";
//                        for (int i=0; i < scanRecord.length; i++) {
//                            record += String.format("0x%02X ",scanRecord[i]);
//                        }
                        if (!ADVERTISERS.containsKey(device.getAddress())) {
                            float[] pos = new float[3];
                            ADVERTISERS.put(device.getAddress(), new Advertiser(device.getAddress(), pos, new Integer(scanRecord[29]).intValue()));
                        }

                        ADVERTISERS.get(device.getAddress()).add_measurement(rssi);


                        List<String> keys = new ArrayList(ADVERTISERS.keySet());
                        Collections.sort(keys);
                        for (String e : keys) {
                            Log.d(LOGTAG, ADVERTISERS.get(e).toString());
                        }
                    }
                });
            }
        });

    }

    /**
     * Class get called from nelder-mead
     */
    private static class myfunc extends Empty implements MultivariateFunction {
        /**
         * This should be error-function as of python-code
         * @param x
         * @return
         */
        public double value(double[] x) {
            ArrayList<float[]> advers = getBeacon_positions();
            ArrayList<Float> dist = getS();
            ArrayList<Float> dist_er = getBeacon_variance();
            ArrayList<Double> scaling = new ArrayList<Double>();


            List<float[]> keys = new ArrayList<float[]>(advers);
            ArrayList<Float> tmp = new ArrayList<Float>();

            String dbg = "";
            // Print out given x,y,z
/*            for (int i = 0; i < x.length; i++) {
                dbg += "x["+Integer.toString(i)+"]:" + Double.toString(x[i]) +" ";
            }
            System.out.println(dbg);
            dbg = null;*/

            for (float[] element: keys) {
                // Python: (x-beacon_positions)**2
                double new_x = Math.pow(x[0] - element[0], 2);
                double new_y = Math.pow(x[1] - element[1], 2);
                double new_z = Math.pow(x[2] - element[2], 2);


                // Python: (((x-beacon_positions)**2).sum(axis=1))
                double sum = new_x + new_y + new_z;

                // Python: ((x-beacon_positions)**2).sum(axis=1))**0.5
                tmp.add( (float) Math.pow(sum, 0.5) );
            }
            //tmp is now r

            //dbg r
/*    		for (Float e: tmp) {
    			System.out.println("r: " + Float.toString(e));
    		}*/


            for(int i = 0; i < tmp.size(); i++) {
                // Python: (s-r)**2
                double er = Math.pow(  dist.get(i) - tmp.get(i), 2 );
                //dbg
                //System.out.println("error: " + Double.toString(er));

                // Python:  np.fmin(errors, 0*r+1**2)
                er = Math.min(er, 1.);
                //dbg
                //System.out.println("errors:" + Double.toString(er));
                tmp.set(i, (float) er);
            }
            // tmp is now error




            // Scalling
/*    		for (Double e: dist_er) {
    			// 1/0.0 will produce "Infinity"
    			double scaling_tmp = 1/e.doubleValue();
    			if (scaling_tmp == Double.NEGATIVE_INFINITY || scaling_tmp == Double.POSITIVE_INFINITY) {
    				scaling_tmp = -0.000000001;
    			}
    			scaling.add( scaling_tmp );
    		}*/

            // Sum up with scaling
/*    		double ret = 0.;
    		for(int i = 0; i < tmp.size(); i++) {
    			ret = ret + (tmp.get(i) * scaling.get(i));
    		}*/

            double ret = 0;
            for(Float e: tmp) {
                ret = ret + e;
            }

            //System.out.println("ret: "+ Double.toString(ret));
            return ret;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.empty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public float[] vec_sub(float[] a, float[] b) {
        float[] c = new float[3];

        //TODO Exception wenn nicht beide Arrays 3 Elemente
        for (int i = 0; i <= a.length-1; i++) {
            c[i] = a[i] - b[i];
        }

        return c;
    }

    /**
     * Find Advertiser with lowest distance
     *
     * @return Advertiser with lowest distance
     */
    private Advertiser advertiser_min_distance() {
        // Build a very far Advertiser to be sure to not be the one with the lowest distance
        Advertiser lowest = new Advertiser("", new float[]{0.0f, 0.0f, 0.0f}, 1).add_measurement(999).add_measurement(999);
        Advertiser cur;

        List<String> keys = new ArrayList(ADVERTISERS.keySet());
        for (String e : keys) {
            cur = ADVERTISERS.get(e);
            lowest = cur.get_distance() < lowest.get_distance() ? cur : lowest;
        }
        return lowest;
    }

    /**
     * Gets called by pressing Button
     * 1th version should print postion via Log.d
     * Final version should set marker on map
     */
    private void get_position() {

        // gerneriere daten zur simulation
        simulationsdaten();

        List<String> keys = new ArrayList(ADVERTISERS.keySet());
        for (String e : keys) {
            Advertiser cur = ADVERTISERS.get(e);
            s.add(cur.get_distance());
            beacon_variance.add( cur.get_distance_error() );
            beacon_positions.add( cur.getPos() );
        }

        sx = vec_sub(advertiser_min_distance().getPos(), new float[]{0.0f, 0.0f, 0.2f});



        SimplexOptimizer optimizer = new SimplexOptimizer(1e-10, 1e-30);
        optimizer.setSimplex(new NelderMeadSimplex(new double[] { 0.15, 0.15, 0.15}));
        PointValuePair optimum
                = optimizer.optimize(5000, new myfunc(), GoalType.MINIMIZE, new double[] { 3., 3., 0.3});
        double x = optimum.getPoint()[0];
        double y = optimum.getPoint()[1];
        double z = optimum.getPoint()[2];



        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast.makeText(context, String.format("Magic says x:%f y:%f z:%f", x, y, z), duration).show();

        Log.d(LOGTAG, String.format("Magic says x:%f y:%f z:%f", x, y, z));
    }

    public void simulationsdaten() {
        ADVERTISERS.clear();

        ADVERTISERS.put("00:07:80:52:64:e6", new Advertiser("00:07:80:52:64:e6", new float[]{2.5f, 5f, 1f}, 2).add_measurement(1).add_measurement(1) );
        ADVERTISERS.put("00:07:80:7e:c3:68", new Advertiser("00:07:80:7e:c3:68", new float[]{1.0f, 0.5f, 0.5f}, 2).add_measurement(1).add_measurement(1) );
        ADVERTISERS.put("00:07:80:7e:c3:7b", new Advertiser("00:07:80:7e:c3:7b", new float[]{5f, 2f, 1f}, 2).add_measurement(1).add_measurement(1) );
        ADVERTISERS.put("00:07:80:68:1c:9c", new Advertiser("00:07:80:68:1c:9c", new float[]{0.5f, .5f, 2.2f}, 2).add_measurement(1).add_measurement(1) );
        ADVERTISERS.put("00:07:80:68:28:29", new Advertiser("00:07:80:68:28:29", new float[]{6f, 5f, 1f}, 2).add_measurement(1).add_measurement(1) );
        ADVERTISERS.put("00:07:80:68:28:67", new Advertiser("00:07:80:68:28:67", new float[]{3f, 3f, 0.5f}, 2).add_measurement(1).add_measurement(1) );
        ADVERTISERS.put("00:07:80:79:1f:f1", new Advertiser("00:07:80:79:1f:f1", new float[]{6f, .5f, 1.8f}, 2).add_measurement(1).add_measurement(1) );
        ADVERTISERS.put("00:07:80:c0:ff:ee", new Advertiser("00:07:80:c0:ff:ee", new float[]{0.5f, 5f, 1f}, 2).add_measurement(1).add_measurement(1) );
    }


    public ArrayList<Float> getS() {
        return s;
    }

    public ArrayList<Float> getBeacon_variance() {
        return beacon_variance;
    }

    public ArrayList<float[]> getBeacon_positions() {
        return beacon_positions;
    }

    public float[] getSx() {
        return sx;
    }

    public Hashtable<String, Advertiser> getADVERTISERS() {
        return ADVERTISERS;
    }
}

