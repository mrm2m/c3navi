package so.libcrypt.testapp.testapp;

import android.util.Log;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by janitor on 25.09.14.
 */
public class Advertiser {
    public static final String LOGTAG = "31c3";
    private final int OLD_LMT = 3000;
    private final float C = -(float) Math.log(10) / 20;

    private String mac;
    private float[] pos;
    private int tx;
    private HashMap<Date, Integer> rssi = new HashMap<Date, Integer>();

    /**
     * Contructor
     *
     * @param mac MAC-Adress of beacon
     * @param pos Float-Array for X,Y,Z
     * @param tx  Int, TX-Power
     */
    public Advertiser(String mac, float[] pos, int tx) {
        this.mac = mac;
        this.pos = pos;
        this.tx = tx;
    }


    /**
     * Remove old Measurements
     *
     * @param old time delta in milliseconds
     */
    public void clean_measurements(int old) {
        Date now = new Date();

        Iterator<Map.Entry<Date, Integer>> iter = this.rssi.entrySet().iterator();

        while (iter.hasNext()) {
            Map.Entry<Date, Integer> entry = iter.next();
            if (now.getTime() - entry.getKey().getTime() > old) {
                iter.remove();
            }
        }
    }

    /**
     * Adds new rssi
     * clears old entrys before adding
     *
     * @param add_rssi
     */
    public Advertiser add_measurement(int add_rssi) {
        this.clean_measurements(OLD_LMT);
        this.rssi.put(new Date(), add_rssi);
        return this;
    }


    private float get_middled_rssi() {
        this.clean_measurements(OLD_LMT);

        //Log.d(LOGTAG, "rssi:" + this.rssi.values().toString());

        if (this.rssi.size() != 0) {
            int res = 0;
            for (Integer entry : this.rssi.values()) {
                res += entry.floatValue();
            }
            return res / this.rssi.size();
        } else {
            return -1000;
        }
    }


    private float get_rssi_error() {
        if (this.rssi.size() >= 2) {
            float m = this.get_middled_rssi();
            double res = 0.;

            for (Integer v : this.rssi.values()) {
                res += Math.pow((double) v.floatValue() - m, (double) 2);
            }

            return (float) res / ((float) this.rssi.size() - 1);
        } else {
            return -1000;
        }
    }

    public float get_distance() {
        if (this.get_middled_rssi() != -1000) {
            float ratio_power = this.tx - this.get_middled_rssi();

            return (float) Math.pow(Math.pow((double) 10, (double) ratio_power / 10.), 0.5);
        } else {
            return -1000;
        }
    }

    public float get_distance_error() {
        if (this.get_distance() == -1000 || this.get_rssi_error() == -1000) {
            return -1000;
        } else {
            return C * this.get_distance() * this.get_rssi_error();
        }
    }


    // Our own .toString :D
    @Override
    public String toString() {
        try {
            String s = String.format("%s: (%.1f|%.1f|%d) %.5f+-%.1f", this.mac,
                    this.get_middled_rssi(),
                    this.get_rssi_error(),
                    this.tx,
                    this.get_distance(), // -1000,000000
                    this.get_distance_error());
            return s;
        } catch (Exception e) {
            return "" + this.mac + ": N/A! " + e.getMessage();
        }
    }

    public int compareTo(Advertiser adv) {
        // Anderer ist groesser als ich
        return (int) Math.round((double) 1000 * (this.get_middled_rssi() - adv.get_middled_rssi()));
    }

    /**
     * Getter of pos
     * @return position of Advertiser
     */
    public float[] getPos() {
        return this.pos;
    }
}
